package com.squircle.image;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.squircleview.SquircleView;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

public class SquircleViewManager extends SimpleViewManager<SquircleView> {

    private ThemedReactContext mContext;

    @Override
    public String getName() {
        return SquircleImageModule.NAME;
    }

    @Override
    protected SquircleView createViewInstance(ThemedReactContext reactContext) {
        this.mContext = reactContext;
        return new SquircleView(reactContext);
    }

    @ReactProp(name = "source")
    public void setImageSrc(final SquircleView image, ReadableMap source) {
        // 加载url对应的图片
        if (null != source && !source.getString("uri").isEmpty()) {
            Glide.with(mContext)
                    .load(source.getString("uri"))
                    .centerCrop()
                    .into(image);
        }
    }

}
