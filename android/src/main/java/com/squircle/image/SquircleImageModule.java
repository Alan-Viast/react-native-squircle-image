
package com.squircle.image;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

public class SquircleImageModule extends ReactContextBaseJavaModule {

    static final String NAME = "SquircleImage";

    private final ReactApplicationContext reactContext;

    public SquircleImageModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return NAME;
    }
}