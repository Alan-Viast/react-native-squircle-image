import React, { Component } from 'react';
// native component & bridge helpers
import { View, requireNativeComponent } from 'react-native';

var iface = {
  name: 'SquircleImage',
  propTypes: {
    ...View.propTypes // include the default view properties
  }
};

const WrappedSquircleImage = requireNativeComponent('SquircleImage', iface);

export default class SquircleImage extends Component {
  render() {
    return (
      <WrappedSquircleImage {...this.props} />
  );
  }
}
