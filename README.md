
# react-native-react-native-squircle-image

## Getting started

`$ npm install react-native-react-native-squircle-image --save`

### Mostly automatic installation

`$ react-native link react-native-react-native-squircle-image`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-react-native-squircle-image` and add `RNReactNativeSquircleImage.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNReactNativeSquircleImage.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.squircle.image.RNReactNativeSquircleImagePackage;` to the imports at the top of the file
  - Add `new RNReactNativeSquircleImagePackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-react-native-squircle-image'
  	project(':react-native-react-native-squircle-image').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-react-native-squircle-image/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-react-native-squircle-image')
  	```


## Usage
```javascript
import RNReactNativeSquircleImage from 'react-native-react-native-squircle-image';

// TODO: What to do with the module?
RNReactNativeSquircleImage;
```
  