
Pod::Spec.new do |s|
  package = JSON.parse(File.read(File.join(__dir__, 'package.json')))

  s.name         = "RNReactNativeSquircleImage"
  s.version       = package['version']
  s.summary       = package['description']
  s.description  = <<-DESC
                  RNReactNativeSquircleImage
                   DESC
  s.homepage     = "https://gitlab.com/Alan-Viast/react-native-squircle-image"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "AlanViast@gmail.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://gitlab.com/Alan-Viast/react-native-squircle-image.git", :tag => "master" }
  s.source_files  = "ios/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end
